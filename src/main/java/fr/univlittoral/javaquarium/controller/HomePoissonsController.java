package fr.univlittoral.javaquarium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import fr.univlittoral.javaquarium.dto.PoissonDTO;
import fr.univlittoral.javaquarium.service.PoissonService;


@RestController
@RequestMapping(value = "/api/poissons")
public class HomePoissonsController {
	
	@Autowired
	private PoissonService pservice;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<PoissonDTO> AllPoissons(){
		
		return pservice.getAll();
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public void addPoisson(@RequestBody final PoissonDTO dto) {
		
		pservice.addPoisson(dto);
		
	}
	
	
}
