package fr.univlittoral.javaquarium.dao;

import java.util.ArrayList;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.Entity.PoissonDO;

@Component
public class PoissonDAO {
	@Autowired
	private EntityManager em;
	
	
	/*public void createPoissons(){
		
		PoissonDO newPoissons = new PoissonDO();
		em.persist(newPoissons);
		
	}*/
	
	public ArrayList<PoissonDO> getAll(){
		System.out.println("SELECT p FROM PoissonDO p");
		Query query = em.createQuery("SELECT p FROM PoissonDO p");
		return (ArrayList<PoissonDO>) query.getResultList();
		
	}
	
	public void addPoisson(PoissonDO poisson) {
		em.persist(poisson);
	}
	
}
