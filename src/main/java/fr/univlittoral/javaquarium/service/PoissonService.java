package fr.univlittoral.javaquarium.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import fr.univlittoral.javaquarium.Entity.PoissonDO;
import fr.univlittoral.javaquarium.dao.PoissonDAO;
import fr.univlittoral.javaquarium.dto.PoissonDTO;

@Component
public class PoissonService {
	@Autowired
	private PoissonDAO dao;
	final List<PoissonDTO> AllPoissons = new ArrayList<>();
	private static final String SEPARATOR_DIM = "x";
	
	public List<PoissonDTO> getAll() {
		final List<PoissonDO> poissons = dao.getAll();

		for (int i = 0; i < poissons.size(); i++) {
			PoissonDO pdo = poissons.get(i);
			final PoissonDTO poissonDTO = new PoissonDTO();
			String description;
			description  = pdo.getDesc1() != null ? pdo.getDesc1() : "";
			description += pdo.getDesc2() != null ? "\n" + pdo.getDesc2() : "";
			description += pdo.getDesc3() != null ? "\n" + pdo.getDesc3() : "";
			poissonDTO.setNom(pdo.getEspece());
			poissonDTO.setDescription(description);
			poissonDTO.setCouleur(pdo.getCouleur());
			poissonDTO.setDimension(pdo.getLargeur() + SEPARATOR_DIM + pdo.getLongueur());
			poissonDTO.setPrix(pdo.getPrix());
			AllPoissons.add(poissonDTO);
		}
		return AllPoissons;
	}
	
	public void addPoisson(final PoissonDTO dto) {
				PoissonDO pdo = new PoissonDO();
				String[] dimensions = dto.getDimension().split(SEPARATOR_DIM);
				pdo.setEspece(dto.getNom());
				pdo.setDesc1(dto.getDescription());
				pdo.setDesc2(dto.getDescription());
				pdo.setDesc3(dto.getDescription());
				pdo.setCouleur(dto.getCouleur());
				pdo.setLargeur(Float.parseFloat(dimensions[0]));
				pdo.setLongueur(Float.parseFloat(dimensions[1]));
				pdo.setPrix(dto.getPrix());
				dao.addPoisson(pdo);
	}
	
}
